#ifndef LIBBLING_H
#define LIBBLING_H

#define BLING_OWNER(T) T
#define BLING_STRING   char*
#define BLING_CSTRING  char const*

typedef enum BlingTAttr {
	Text_Default = 1 << 0,
	Text_Bold    = 1 << 1,
	Text_UScore  = 1 << 2,
	Text_Blink   = 1 << 3,
	Text_Invert  = 1 << 4,
	Text_Hidden  = 1 << 5,
} BlingTAttr;

typedef enum BlingColor {
	Text_Black	 = 0,
	Text_Red	 = 1,
	Text_Green	 = 2,
	Text_Yellow	 = 3,
	Text_Blue	 = 4,
	Text_Magenta = 5,
	Text_Cyan	 = 6,
	Text_White 	 = 7,
} BlingColor;

BLING_OWNER(BLING_STRING) bling_curloc(int, int);

BLING_OWNER(BLING_STRING) bling_cur_fw(int);
BLING_OWNER(BLING_STRING) bling_cur_bw(int);
BLING_OWNER(BLING_STRING) bling_cur_uw(int);
BLING_OWNER(BLING_STRING) bling_cur_dw(int);

BLING_OWNER(BLING_STRING) bling_cls();
BLING_OWNER(BLING_STRING) bling_textattr(BlingTAttr);
BLING_OWNER(BLING_STRING) bling_fg_color(BlingColor);
BLING_OWNER(BLING_STRING) bling_bg_color(BlingColor);

#endif//LIBBLING_H