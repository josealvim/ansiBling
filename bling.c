#include "bling.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

// :^)
#define BLING_RETURN_ESEQ(fmt, ...)					\
	int strs = snprintf(NULL, 0, fmt, __VA_ARGS__);	\
	char *eseq = malloc(strs + 1);					\
	sprintf(eseq, fmt, __VA_ARGS__);				\
	return eseq;

BLING_OWNER(BLING_STRING) 
bling_curloc(int li, int co) {
	BLING_RETURN_ESEQ("\e[%d;%dH", li, co);
}

BLING_OWNER(BLING_STRING) bling_cur_uw(int n) {
	BLING_RETURN_ESEQ("\e[%dA", n);
}

BLING_OWNER(BLING_STRING) bling_cur_dw(int n) {
	BLING_RETURN_ESEQ("\e[%dB", n);
}

BLING_OWNER(BLING_STRING) bling_cur_fw(int n) {
	BLING_RETURN_ESEQ("\e[%dC", n);
}

BLING_OWNER(BLING_STRING) bling_cur_bw(int n) {
	BLING_RETURN_ESEQ("\e[%dD", n);
}

BLING_OWNER(BLING_STRING) bling_cls() {
	return strdup("\e[2J");
}

static 
BLING_CSTRING _bling_textattr(BlingTAttr attr) {
	switch (attr) {
		case Text_Default: return "\e[0m";
		case Text_Bold	 : return "\e[1m";
		case Text_UScore : return "\e[4m";
		case Text_Blink	 : return "\e[5m";
		case Text_Invert : return "\e[7m";
		case Text_Hidden : return "\e[8m";
		default: 
			return "";
	}
}

BLING_OWNER(BLING_STRING) bling_textattr(BlingTAttr attr) {
	BlingTAttr save = attr;

	int buffsize = 1;
	for (int bit = 1; bit <= attr; bit <<= 1) {
		buffsize+= strlen(_bling_textattr(attr & bit));
	}

	char *buffer = malloc(buffsize);
	char *bufpos = buffer;

	for (int bit = 1; bit <= attr; bit <<= 1) {
		char const *mode = _bling_textattr(attr & bit);
		strcat(bufpos, mode);
		bufpos += strlen(mode);
	}

	return buffer;	
}

BLING_OWNER(BLING_STRING) bling_fg_color(BlingColor c) {
	BLING_RETURN_ESEQ("\e[%dm", c+30);
}

BLING_OWNER(BLING_STRING) bling_bg_color(BlingColor c) {
	BLING_RETURN_ESEQ("\e[%dm", c+40);
}