.PHONY: clean lib

lib: bling.o
	@echo done !

clean:
	rm -rf *.o *.so

%.o: %.c
	gcc -I. -o $@ -c $<